import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductsDto } from './dto/create-products.dto';
import { UpdateProductsDto } from './dto/update-products.dto';
import { Products } from './entities/products.entity';

let products: Products[] = [
  { id: 1, name: 'Administrator', price: 100 },
  { id: 2, name: 'User 1', price: 200 },
  { id: 3, name: 'User 2', price: 300 },
];

let lastProductsId = 4;
@Injectable()
export class ProductsService {
  create(createProductsDto: CreateProductsDto) {
    const newProducts: Products = {
      id: lastProductsId++,
      ...createProductsDto, // id, name, price
    };
    products.push(newProducts);
    return newProducts;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductsDto: UpdateProductsDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user ' + JSON.stringify(products[index]));
    // console.log('update' + JSON.stringify(updateProductsDto));
    const updateProduct: Products = {
      ...products[index], // id, name, login, password
      ...updateProductsDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }

    const deleteUser = products[index];
    products.splice(index, 1);
    return deleteUser;
  }

  reset() {
    products = [
      { id: 1, name: 'Administrator', price: 100 },
      { id: 2, name: 'User 1', price: 200 },
      { id: 3, name: 'User 2', price: 300 },
    ];
    lastProductsId = 4;
    return 'RESET';
  }
}
